USE [BlogDB]
GO
/****** Object:  Table [dbo].[Kullanici]    Script Date: 17.12.2016 21:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kullanici](
	[KullaniciId] [int] IDENTITY(1,1) NOT NULL,
	[AdSoyad] [nvarchar](64) NULL,
	[Email] [nvarchar](64) NULL,
	[Sifre] [nvarchar](32) NULL,
 CONSTRAINT [PK_Kullanici] PRIMARY KEY CLUSTERED 
(
	[KullaniciId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Makale]    Script Date: 17.12.2016 21:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Makale](
	[MakaleId] [int] IDENTITY(1,1) NOT NULL,
	[MakaleBaslik] [nvarchar](64) NULL,
	[MakaleBaslikEn] [nvarchar](64) NULL,
	[MakaleOzet] [nvarchar](256) NULL,
	[MakaleOzetEn] [nvarchar](256) NULL,
	[MakaleDetay] [nvarchar](max) NULL,
	[MakaleDetayEn] [nvarchar](max) NULL,
	[EklenmeTarihi] [datetime] NULL CONSTRAINT [DF_Makale_EklenmeTarihi]  DEFAULT (getdate()),
	[KullaniciId] [int] NULL,
 CONSTRAINT [PK_Makale] PRIMARY KEY CLUSTERED 
(
	[MakaleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Yorum]    Script Date: 17.12.2016 21:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Yorum](
	[YorumId] [int] IDENTITY(1,1) NOT NULL,
	[AdSoyad] [nvarchar](64) NULL,
	[YorumDetay] [nvarchar](max) NULL,
	[YorumTarih] [datetime] NULL CONSTRAINT [DF_Yorum_YorumTarih]  DEFAULT (getdate()),
	[MakaleId] [int] NULL,
 CONSTRAINT [PK_Yorum] PRIMARY KEY CLUSTERED 
(
	[YorumId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Kullanici] ON 

INSERT [dbo].[Kullanici] ([KullaniciId], [AdSoyad], [Email], [Sifre]) VALUES (2, N'Kullanıcı', N'test@deneme.com', N'123')
SET IDENTITY_INSERT [dbo].[Kullanici] OFF
SET IDENTITY_INSERT [dbo].[Makale] ON 

INSERT [dbo].[Makale] ([MakaleId], [MakaleBaslik], [MakaleBaslikEn], [MakaleOzet], [MakaleOzetEn], [MakaleDetay], [MakaleDetayEn], [EklenmeTarihi], [KullaniciId]) VALUES (7, N'Merhaba Dünya', N'Hello World', N'Selam Ben Tahir', N'Hello, My Name is Tahir', N'<strong style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" text-align:="" justify;"="">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" text-align:="" justify;"="">, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500''lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. Beşyüz yıl boyunca varlığını sürdürmekle kalmamış, aynı zamanda</span>', N'<strong style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: "Open Sans", Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: "Open Sans", Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;"><span class="Apple-converted-space"> </span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</span>', CAST(N'2016-12-15 00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Makale] ([MakaleId], [MakaleBaslik], [MakaleBaslikEn], [MakaleOzet], [MakaleOzetEn], [MakaleDetay], [MakaleDetayEn], [EklenmeTarihi], [KullaniciId]) VALUES (21, N'Makale Dünyası', N'Article World', N'Makaleler Gündelik Yaşamımızın çok büyük bir kısmını etkiliyor bence', N'I think the articles affect a great deal of our Everyday Life', N'<p><span style="color: rgb(0, 0, 0); font-family: "Open Sans", Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Yinelenen bir sayfa içeriğinin okuyucunun dikkatini dağıttığı bilinen bir gerçektir. Lorem Ipsum kullanmanın amacı, sürekli ''buraya metin gelecek, buraya metin gelecek'' yazmaya kıyasla daha dengeli bir harf dağılımı sağlayarak okunurluğu artırmasıdır. Şu anda birçok masaüstü yayıncılık paket</span></p>', N'<p><span style="color: rgb(0, 0, 0); font-family: "Open Sans", Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''C</span></p>', CAST(N'2016-12-17 00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Makale] ([MakaleId], [MakaleBaslik], [MakaleBaslikEn], [MakaleOzet], [MakaleOzetEn], [MakaleDetay], [MakaleDetayEn], [EklenmeTarihi], [KullaniciId]) VALUES (22, N'29 Ekim', N'29 October', N'Cumhuriyet Bayramı', N'Cumhuriyet Feast', N'<p><span style="color: rgb(0, 0, 0); font-family: "Open Sans", Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Yaygın
 inancın tersine, Lorem Ipsum rastgele sözcüklerden oluşmaz. Kökleri 
M.Ö. 45 tarihinden bu yana klasik Latin edebiyatına kadar uzanan 2000 
yıllık bir geçmişi vardır. Virginia''daki Hampden-Sydney College''dan 
Latince profesörü Richard McClintock, bir L</span></p>', N'<span style="color: rgb(0, 0, 0); font-family: "Open Sans", Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum</span>', CAST(N'2016-12-17 20:57:21.747' AS DateTime), 2)
SET IDENTITY_INSERT [dbo].[Makale] OFF
SET IDENTITY_INSERT [dbo].[Yorum] ON 

INSERT [dbo].[Yorum] ([YorumId], [AdSoyad], [YorumDetay], [YorumTarih], [MakaleId]) VALUES (30, N'Mehmet', N'bugün 29 ekim :))', CAST(N'2016-12-17 21:05:49.557' AS DateTime), 22)
INSERT [dbo].[Yorum] ([YorumId], [AdSoyad], [YorumDetay], [YorumTarih], [MakaleId]) VALUES (31, N'Ayşe', N'Selam makale için teşekkürler', CAST(N'2016-12-17 21:07:30.523' AS DateTime), 21)
INSERT [dbo].[Yorum] ([YorumId], [AdSoyad], [YorumDetay], [YorumTarih], [MakaleId]) VALUES (32, N'Tomas', N'Hi.! My name is Tomas.', CAST(N'2016-12-17 21:08:11.487' AS DateTime), 7)
SET IDENTITY_INSERT [dbo].[Yorum] OFF
ALTER TABLE [dbo].[Makale]  WITH CHECK ADD  CONSTRAINT [FK_Makale_Kullanici1] FOREIGN KEY([KullaniciId])
REFERENCES [dbo].[Kullanici] ([KullaniciId])
GO
ALTER TABLE [dbo].[Makale] CHECK CONSTRAINT [FK_Makale_Kullanici1]
GO
ALTER TABLE [dbo].[Yorum]  WITH CHECK ADD  CONSTRAINT [FK_Yorum_Makale] FOREIGN KEY([MakaleId])
REFERENCES [dbo].[Makale] ([MakaleId])
GO
ALTER TABLE [dbo].[Yorum] CHECK CONSTRAINT [FK_Yorum_Makale]
GO
