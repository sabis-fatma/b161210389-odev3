﻿using System;
using System.Linq;
using System.Web.Mvc;
using BlogApp.Models;

namespace BlogApp.Controllers
{
    public class LanguageController : Controller
    {

        private readonly BlogDBEntities _context;

        public LanguageController()
        {
            _context = new BlogDBEntities();
        }
        // GET: Language
        public ActionResult Index()
        {
            var liste = _context.Makale.OrderByDescending(x => x.EklenmeTarihi).ToList();
            return View(liste);
        }


        public ActionResult LatestArticle()
        {
            var liste = _context.Makale.OrderByDescending(x => x.EklenmeTarihi).Take(5).Take(5).ToList();
            return View(liste);
        }


        public ActionResult ArticleDetail(int id)
        {
            var oku = _context.Makale.Find(id);
            return View(oku);
        }


        [HttpPost]
        public ActionResult ArticleDetail(FormCollection frm)
        {
            Yorum yorum = new Yorum();

            yorum.MakaleId = Convert.ToInt32(frm["MakaleId"].Trim());
            yorum.AdSoyad = frm["adsoyad"].Trim();
            yorum.YorumDetay = frm["yorumdetay"].Trim();
            yorum.YorumTarih = DateTime.Now;

            _context.Yorum.Add(yorum);
            _context.SaveChanges();
            return Redirect("/Language/ArticleDetail/" + yorum.MakaleId);
        }

       

        public ActionResult Commend(int id)
        {
            var makale = _context.Makale.Find(id);
            return View(makale.Yorum.ToList());
        }


    }
}