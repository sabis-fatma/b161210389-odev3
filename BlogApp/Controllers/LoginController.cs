﻿using System.Web.Mvc;
using BlogApp.Models;



namespace BlogApp.Controllers
{

    public class LoginController : Controller
    {
        private readonly BlogDBEntities _context;

        public LoginController()
        {
            _context = new BlogDBEntities();
        }


        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Index(Kullanici kullanici)
        {
            var kullaniciKontrol = new KullaniciKontrol();
            var kontrol = kullaniciKontrol.IsLoginSuccess(kullanici.Email, kullanici.Sifre);

            if (kontrol)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult LogOff()
        {
            Session["UserInfo"] = null;
            return RedirectToAction("Index", "Home");
        }


        public ActionResult KayitOl()
        {
            return View();
        }

        [HttpPost]
        public ActionResult KayitOl(Kullanici kullanici)
        {
            _context.Kullanici.Add(kullanici);
            _context.SaveChanges();
            return RedirectToAction("Index","Login");
        }









    }
}

