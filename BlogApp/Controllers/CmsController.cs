﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;
using BlogApp.Models;



namespace BlogApp.Controllers
{
    [KullaniciGiris]
    public class CmsController : Controller
    {
        private readonly BlogDBEntities _context;

        public CmsController()
        {
            _context = new BlogDBEntities();
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MakaleListesi()
        {
            var liste = _context.Makale.OrderByDescending(x => x.EklenmeTarihi).ToList();
            return View(liste);
        }
        [HttpGet]
        public ActionResult MakaleEkle()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MakaleEkle(Makale makale)
        {

            makale.EklenmeTarihi = DateTime.Now;
            _context.Makale.Add(makale);
            _context.SaveChanges();

            return RedirectToAction("MakaleListesi");

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MakaleGuncelle(Makale makale)
        {
            var guncelle = _context.Makale.Find(makale.MakaleId);
            guncelle.MakaleBaslik = makale.MakaleBaslik;
            guncelle.MakaleBaslikEn = makale.MakaleBaslikEn;
            guncelle.MakaleOzet = makale.MakaleOzet;
            guncelle.MakaleOzetEn = makale.MakaleOzetEn;
            guncelle.MakaleDetay = makale.MakaleDetay;
            guncelle.MakaleDetayEn = makale.MakaleDetayEn;

            _context.Makale.AddOrUpdate(guncelle);

            _context.SaveChanges();
            return RedirectToAction("MakaleListesi");

        }

        public ActionResult MakaleGit(int id)
        {
            var git = _context.Makale.Find(id);
            var makale = new Makale
            {
                MakaleId = git.MakaleId,
                MakaleBaslik = git.MakaleBaslik,
                MakaleBaslikEn = git.MakaleBaslikEn,
                MakaleOzet = git.MakaleOzet,
                MakaleOzetEn = git.MakaleOzetEn,
                MakaleDetay = git.MakaleDetay,
                MakaleDetayEn = git.MakaleDetayEn,

            };
            return View(makale);
        }





        public ActionResult MakaleSil(int id)
        {
            var makale = _context.Makale.Find(id);
            _context.Makale.Remove(makale);

            _context.SaveChanges();
            return RedirectToAction("MakaleListesi");
        }





        public ActionResult YorumListesi()
        {
            return View();
        }



    }
}