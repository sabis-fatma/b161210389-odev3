﻿using System;
using System.Linq;
using System.Web.Mvc;
using BlogApp.Models;

namespace BlogApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly BlogDBEntities _context;

        public HomeController()
        {
            _context = new BlogDBEntities();
        }



        public ActionResult Index()
        {
            var liste = _context.Makale.OrderByDescending(x => x.EklenmeTarihi).ToList();
            return View(liste);
        }


        public ActionResult SonMakale()
        {
            var liste = _context.Makale.OrderByDescending(x => x.EklenmeTarihi).Take(5).Take(5).ToList();
            return View(liste);
        }

        public ActionResult MakaleDetay(int id)
        {
            var oku = _context.Makale.Find(id);
            return View(oku);
        }

        [HttpPost]
        public ActionResult MakaleDetay(FormCollection frm)
        {
           Yorum yorum = new Yorum();
           
            yorum.MakaleId = Convert.ToInt32(frm["MakaleId"].Trim());
            yorum.AdSoyad = frm["adsoyad"].Trim();
            yorum.YorumDetay = frm["yorumdetay"].Trim();
            yorum.YorumTarih = DateTime.Now;

            _context.Yorum.Add(yorum);
            _context.SaveChanges();
            return Redirect("/Home/MakaleDetay/" + yorum.MakaleId);
        }

       

        public ActionResult Yorum(int id)
        {
            var makale = _context.Makale.Find(id);
            return View(makale.Yorum.ToList());
        }





    }
}