﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogApp.Models
{
    public class KullaniciKontrol
    {

        private readonly BlogDBEntities _context;

        public KullaniciKontrol()
        {
            _context = new BlogDBEntities();
        }


        public bool IsLoginSuccess(string email, string sifre)
        {

            var kullanici = _context.Kullanici.FirstOrDefault(x => x.Email == email && x.Sifre == sifre);

            if (kullanici != null)
            {

                HttpContext.Current.Session["UserInfo"] = kullanici;

                return true;
            }

            HttpContext.Current.Session["UserInfo"] = null;
            return false;

        }



    }
}