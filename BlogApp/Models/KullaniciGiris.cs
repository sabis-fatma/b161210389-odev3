﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BlogApp.Models
{
    public class KullaniciGiris : ActionFilterAttribute
    {
        private readonly BlogDBEntities _context;

        public KullaniciGiris()
        {
            _context = new BlogDBEntities();
        }
       
            public override void OnActionExecuting(ActionExecutingContext context)
            {
                base.OnActionExecuting(context);

                if (HttpContext.Current.Session["UserInfo"] == null)
                {
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "Home",
                        action = "Index"
                    }));
                }
            }



       


    }
}